import React, { useState, useRef } from "react";

export const Dashboard = () => {

    const [showError, setShowError] = useState(false);
    const [showWarning, setShowWarning] = useState(false);
    const [showWarningDate, setShowWarningDate] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const tecnico = useRef(null);
    const servicio = useRef(null);
    const fechaInicio = useRef(null);
    const fechaFin = useRef(null);

    const guardarReporte = () => {
        setShowWarning(false);
        setShowWarningDate(false);

        if (!tecnico.current.value || !servicio.current.value || !fechaInicio.current.value || !fechaFin.current.value) {
            setShowWarning(true);
            return;
        }

        if((Date.parse(fechaInicio.current.value) >= Date.parse(fechaFin.current.value))){
            setShowWarningDate(true);
            return;
        }

        enviarInformacion();
    }

    const limpiarReporte = () => {
        tecnico.current.value = "";
        servicio.current.value = "";
        fechaInicio.current.value = "";
        fechaFin.current.value = "";
    }

    const enviarInformacion = () => {

        let service = {
            idTecnico: tecnico.current.value, 
            idServicio: servicio.current.value,
            fechaInicio: fechaInicio.current.value,
            fechaFin: fechaFin.current.value
        }

        let url = "http://localhost:8080/servicio";

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(service)
        })
            .then(response => response.json())
            .then(data => {                                
                setShowSuccess(true);
                limpiarReporte();
            }).catch((e) => {
                setShowError(true);
            });
    }

    return (<div>
        <section className="hero is-info">
            <div className="hero-body">
                <div className="container">
                    <h1 className="title">Reporte de servicio</h1>
                </div>
            </div>
        </section>

        <section className="section">
            <div className="container">
                <h2 className="subtitle">Reporte de atención a servicio por técnico:</h2>
                {showWarning && (<div className="notification is-warning">Todos los campos son requeridos</div>)}
                {showWarningDate && (<div className="notification is-warning">La fecha final debe ser mayor que la fecha inicial</div>)}
                {showError && (<div className="notification is-error">Ocurrió un error inesperado</div>)}
                {showSuccess && (<div className="notification is-success">Registro creado exitosamente</div>)}
                <div className="columns">
                    <div className="column">
                        <div className="field">
                            <label className="label">Técnico</label>
                            <div className="control">
                                <input id="idTecnico" className="input" type="text" ref={tecnico}
                                    placeholder="Identificación del técnico" />
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div className="field">
                            <label className="label">Servicio</label>
                            <div className="control">
                                <input id="idServicio" className="input" type="text" ref={servicio}
                                    placeholder="Identificación del servicio" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns">
                    <div className="column">
                        <div className="field">
                            <label className="label">Fecha inicial</label>
                            <div className="control">
                                <input id="fechaInicio" className="input" type="datetime-local" ref={fechaInicio}
                                    placeholder="Text input" />
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div className="field">
                            <label className="label">Fecha final</label>
                            <div className="control">
                                <input id="fechaFin" className="input" type="datetime-local" ref={fechaFin}
                                    placeholder="Text input" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="field is-grouped is-grouped-right">
                    <div className="control">
                        <button id="btnLimpiar" className="button is-secondary is-outlined"
                            onClick={limpiarReporte}>Limpiar</button>
                    </div>
                    <div className="control">
                        <button id="btnGuardarReporte" className="button is-primary"
                            onClick={guardarReporte}>Guardar</button>
                    </div>
                </div>

            </div>
        </section>
    </div>);
}

